package com.devcamp.j60jparelationship.controller;

import java.util.*;

import com.devcamp.j60jparelationship.model.*;
import com.devcamp.j60jparelationship.repository.*;
import com.devcamp.j60jparelationship.service.CarService;
import com.devcamp.j60jparelationship.service.CarTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CarController {
	@Autowired
	private CarService carService;
	@Autowired
	private CarTypeService carTypeService;

	// lấy danh sách country /countries
	@GetMapping("/cars")
	public ResponseEntity<List<CCar>> getAllCars() {
		try {
			return new ResponseEntity<>(carService.getAllCars(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// lấy danh sách carType truyền vào country code /carType?carId=...
	@GetMapping("/carType")
	public ResponseEntity<Set<CCarType>> getCarTypeByCarCode(
			@RequestParam(value = "carId") long car_id) {
		try {
			Set<CCarType> carType = carService.getCarTypeByCarCode(car_id);
			if (carType != null) {
				return new ResponseEntity<>(carType, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/carType-all")
	public ResponseEntity<List<CCarType>> getcarTypeAll() {
		try {
			return new ResponseEntity<>(carTypeService.getAllcarType(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
