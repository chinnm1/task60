package com.devcamp.j60jparelationship.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "car")
public class CCar {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long carId;

	@Column(name = "car_code")
	private String carCode;

	@Column(name = "car_name")
	private String carName;

	@OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Set<CCarType> carType;

	public CCar(long carId, String carCode, String carName, Set<CCarType> carType) {
		this.carId = carId;
		this.carCode = carCode;
		this.carName = carName;
		this.carType = carType;
	}

	public CCar() {
	}

	public long getCarId() {
		return carId;
	}

	public void setCarId(long carId) {
		this.carId = carId;
	}

	public String getCarCode() {
		return carCode;
	}

	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public Set<CCarType> getCarType() {
		return carType;
	}

	public void setCarType(Set<CCarType> carType) {
		this.carType = carType;
	}

}
