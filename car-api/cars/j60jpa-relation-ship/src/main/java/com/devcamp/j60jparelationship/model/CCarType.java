package com.devcamp.j60jparelationship.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "car_type")
public class CCarType {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long type_id;

	@Column(name = "type_code")
	private String typeCode;

	@Column(name = "type_name")
	private String typeName;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "car_id")
	private CCar car;

	public CCarType(long type_id, String typeCode, String typeName, CCar car) {
		this.type_id = type_id;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.car = car;
	}

	public CCarType() {
	}

	public long getType_id() {
		return type_id;
	}

	public void setType_id(long type_id) {
		this.type_id = type_id;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public CCar getCar() {
		return car;
	}

	public void setCar(CCar car) {
		this.car = car;
	}

}
