package com.devcamp.j60jparelationship.repository;

import com.devcamp.j60jparelationship.model.CCar;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICarRepository extends JpaRepository<CCar, Long> {
	CCar findByCarId(long car_id);
}
