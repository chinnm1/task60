package com.devcamp.j60jparelationship.repository;

import com.devcamp.j60jparelationship.model.CCarType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ICarTypeRepository extends JpaRepository<CCarType, Long> {

}
