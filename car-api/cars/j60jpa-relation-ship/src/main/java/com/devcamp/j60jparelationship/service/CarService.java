package com.devcamp.j60jparelationship.service;

import java.util.ArrayList;
import java.util.Set;

import com.devcamp.j60jparelationship.model.CCar;
import com.devcamp.j60jparelationship.model.CCarType;
import com.devcamp.j60jparelationship.repository.ICarRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarService {
    @Autowired
    ICarRepository pCarRepository;

    public ArrayList<CCar> getAllCars() {
        ArrayList<CCar> listCar = new ArrayList<>();
        pCarRepository.findAll().forEach(listCar::add);
        return listCar;
    }

    public Set<CCarType> getCarTypeByCarCode(long car_id) {
        /*
         * List<CRegion> pRegions = new ArrayList<CRegion>();
         * pRegionRepository.findAll().forEach(pRegions::add);
         */
        CCar vCar = pCarRepository.findByCarId(car_id);
        if (vCar != null) {
            return vCar.getCarType();
        } else {
            return null;
        }
    }
}
