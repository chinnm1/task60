package com.devcamp.j60jparelationship.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j60jparelationship.model.CCarType;
import com.devcamp.j60jparelationship.repository.ICarTypeRepository;

import org.springframework.stereotype.Service;

@Service
public class CarTypeService {
    @Autowired
    ICarTypeRepository pCarTypeRepository;

    public ArrayList<CCarType> getAllcarType() {
        ArrayList<CCarType> carType = new ArrayList<>();

        List<CCarType> pcarType = new ArrayList<CCarType>();

        pCarTypeRepository.findAll().forEach(pcarType::add);

        return carType;
    }

}
