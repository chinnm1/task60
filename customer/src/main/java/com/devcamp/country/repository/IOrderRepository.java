package com.devcamp.country.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.country.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {

}
