package com.devcamp.country.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.country.model.CCustomer;
import com.devcamp.country.model.COrder;
import com.devcamp.country.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;

    public ArrayList<CCustomer> getAllCustomer() {
        ArrayList<CCustomer> listCustomer = new ArrayList<>();
        pCustomerRepository.findAll().forEach(listCustomer::add);
        return listCustomer;
    }

    public Set<COrder> getOrderByCustomerId(long customerId) {
        /*
         * List<CRegion> pRegions = new ArrayList<CRegion>();
         * pRegionRepository.findAll().forEach(pRegions::add);
         */
        CCustomer vCustomer = pCustomerRepository.findByCustomerId(customerId);
        if (vCustomer != null) {
            return vCustomer.getOrders();
        } else {
            return null;
        }
    }

}
