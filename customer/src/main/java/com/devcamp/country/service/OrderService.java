package com.devcamp.country.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.country.model.COrder;
import com.devcamp.country.repository.IOrderRepository;

@Service
public class OrderService {
    @Autowired
    IOrderRepository pOrderRepository;

    public ArrayList<COrder> getAllOrder() {
        ArrayList<COrder> regions = new ArrayList<>();

        List<COrder> pRegions = new ArrayList<COrder>();

        pOrderRepository.findAll().forEach(pRegions::add);

        return regions;
    }

}
